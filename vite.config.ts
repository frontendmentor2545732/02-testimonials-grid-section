// vite.config.ts
import UnoCSS from 'unocss/vite'
import { defineConfig } from 'vite'

export default defineConfig({
    base: '/02-testimonials-grid-section',
    plugins: [
        UnoCSS(),
    ],
})
