// uno.config.ts
import {
    defineConfig,
    presetAttributify,
    presetIcons,
    presetTypography, presetUno, presetWebFonts,
    transformerDirectives, transformerVariantGroup
} from 'unocss'

export default defineConfig({
    shortcuts: [
        // ...
    ],
    theme: {
        colors: {
            neutral_light: 'hsl(210, 46%, 95%)',
            neutral_dark: 'hsl(0%, 0%, 81%)',

            accent1_100: 'hsl(264, 55%, 64%)',
            accent1: 'hsl(264, 55%, 52%)',
            accent2: 'hsl(217, 19%, 35%)',
            accent3: 'hsl(219, 29%, 14%)',
        }
    },
    presets: [
        presetUno(),
        presetAttributify(),
        presetIcons(),
        presetTypography(),
        presetWebFonts({
            provider: 'google',
            fonts: {
                'body': 'Barlow Semi Condensed:300,400,500,600'
            },
        }),
    ],
    transformers: [
        transformerDirectives(),
        transformerVariantGroup(),
    ],
})
